import json
import re
from io import StringIO
from typing import List

import requests
from bs4 import BeautifulSoup
from lxml import etree

import logging

logger = logging.getLogger('dota-picker')
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler())
logger.warning('Logger initiated!')


class Hero:
    name_xpath = '/html/body/div[1]/div[8]/div[2]/div[2]/div[1]/div[1]/div[2]/h1'
    popularity_xpath = '/html/body/div[1]/div[8]/div[2]/div[2]/div[1]/div[2]/dl[1]/dd'
    win_rate_xpath = '/html/body/div[1]/div[8]/div[2]/div[2]/div[1]/div[2]/dl[2]/dd/span'
    vs_table_xpath = '/html/body/div[1]/div[8]/div[2]/div[3]/section[3]/article/table/tbody'

    dota_buff_url = 'https://www.dotabuff.com'
    main_url = 'https://www.dotabuff.com/heroes/{}'
    counters_url = 'https://www.dotabuff.com/heroes/{}/counters'

    all_heroes = list()

    def __init__(self, hero_id):
        self.id = hero_id
        # collect main info
        headers = json.load(open('headers.json'))
        response = requests.get(self.main_url.format(hero_id), headers=headers)
        parser = etree.HTMLParser()
        tree = etree.parse(StringIO(response.text), parser)
        self.name, self.role = self._get_name_and_roles(tree)
        self.popularity = self._get_popularity(tree)
        self.win_rate = self._get_win_rate(tree)
        # add self to list of all users
        # self.all_heroes.append(self)
        # load versus tree
        response = requests.get(self.counters_url.format(hero_id), headers=headers)
        versus_tree = etree.parse(StringIO(response.text), parser)
        hero_table = versus_tree.xpath(self.vs_table_xpath)[0]
        self.versus_soup = \
            BeautifulSoup(etree.tostring(hero_table), 'html.parser')

    def _get_name_and_roles(self, tree):
        header = tree.xpath(self.name_xpath)[0]
        soup = BeautifulSoup(etree.tostring(header), 'html.parser')
        name = soup.h1.contents[0]
        roles = soup.h1.small.text.split(', ')
        role = self._predict_role(roles)
        return name, role

    @staticmethod
    def _predict_role(role_list):
        # first role found for hero is most likely his current role
        for _role in role_list:
            if 'Carry' == _role:
                return 'Carry'
            elif 'Support' == _role:
                return 'Support'
            else:
                pass
        return 'Unknown'

    def _get_popularity(self, tree):
        popularity = tree.xpath(self.popularity_xpath)[0]
        soup = BeautifulSoup(etree.tostring(popularity), 'html.parser')
        return soup.dd.text

    def _get_win_rate(self, tree):
        # todo: add win_rate by lane
        win_rate = tree.xpath(self.win_rate_xpath)[0]
        soup = BeautifulSoup(etree.tostring(win_rate), 'html.parser')
        return float(soup.span.text[:-1])/100

    def get_versus(self, versus_ids: List[str]):
        versus_results = dict()
        for row in self.versus_soup.find_all('tr'):
            hero_id = row.get('data-link-to').split('/')[-1]
            if hero_id in versus_ids:
                disadvantage = -float(row.find_all('td')[2].get('data-value'))
                versus_results.update({hero_id: disadvantage})
        return versus_results

    def score(self, versus_ids):
        """
        Basic value for score is hero's win rate.
        If hero is going carry position than it gains bonus coefficient while
        standing against supports, if it goes support than enemy hero has bonus.
        :param versus_ids:
        :return: score
        """
        versus = self.get_versus([h.id for h in versus_ids])
        score = 0
        for hero in versus:
            vs_a = 1
            vs_b = 0
            if self.role == 'Carry':  # and hero.role == 'Support':
                vs_a = 1
                vs_b = 0.05
            elif self.role == 'Support':  # and hero.role == 'Carry':
                vs_a = 0.5
                vs_b = 0
            score += versus[hero] * vs_a + vs_b
        return score / 5.0 + self.win_rate

    def __repr__(self):
        return \
            f"{self.name} " +\
            f"({self.role}) " +\
            f"popularity = {self.popularity} " + \
            f"with WR = {self.win_rate}"


class MatchData:
    url = 'https://www.dotabuff.com/esports/matches'
    table_xpath = '/html/body/div[1]/div[8]/div[2]/div[3]/section/article/table/tbody'

    class Match:
        def __init__(self, match_id: int, radiant_heroes: List[Hero],
                     dire_heroes: List[Hero], winner: str):
            self.id = match_id
            self.radiant = radiant_heroes
            self.dire = dire_heroes
            self.winner = winner

        @staticmethod
        def calculate_team_score(team, enemy):
            logger.debug('Calculating team score...')
            versus = list()
            for _hero in team:
                vs = 1.0 if _hero.role == 'Carry' else 0.9
                versus.append(
                    (_hero.score(enemy) - 0.5) * vs + 0.5
                )
            return sum(versus) / 5.0

        def __repr__(self):
            winner = self.radiant if self.winner == 'radiant' else self.dire
            loser = self.radiant if self.winner == 'dire' else self.dire
            return f"Match {self.id}:\n" +\
                   f"winner ({self.winner}):\n    " +\
                   '\n    '.join([str(hero) for hero in winner]) + "\n" +\
                   "loser:\n    " +\
                   '\n    '.join([str(hero) for hero in loser]) + "\n\n"

    def __init__(self):
        self.matches = list()
        logger.info("Match data app initiated!")

    def get_match_list(self, page_num: int):
        logger.info(f'Collecting match list on page {page_num}...')
        headers = json.load(open('headers.json'))
        data = {'page': page_num}
        response = requests.get(self.url, headers=headers, params=data)
        parser = etree.HTMLParser()
        tree = etree.parse(StringIO(response.text), parser)
        matches_table = tree.xpath(self.table_xpath)[0]
        matches_soup = BeautifulSoup(etree.tostring(matches_table),
                                     'html.parser')
        for _match in matches_soup.find_all('tr'):
            self._collect_match(_match)

    def _collect_match(self, _match):
        _id = int(_match.find_all('td')[2].a.text)
        got_matches = [_match.id for _match in self.matches]
        if _id in got_matches:
            logger.warning(f'Already parsed match {_id}')
            return None
        else:
            logger.info(f'Collecting match {_id}...')
            winner_side = _match.find_all('td')[4].span.get('class')[0]
            dire_col, rad_col = (4, 5) if winner_side == 'dire' else (5, 4)
            dire_pick = list()
            for img in _match.find_all('td')[dire_col].div.find_all('img'):
                dire_pick.append(img.get('src'))
            dire_pick = [
                re.search(r'heroes/.*-', src)[0][7:-1] for src in dire_pick]
            radiant_pick = list()
            for img in _match.find_all('td')[rad_col].div.find_all('img'):
                radiant_pick.append(img.get('src'))
            radiant_pick = [
                re.search(r'heroes/.*-', src)[0][7:-1] for src in radiant_pick]
            self.matches.append(
                self.Match(
                    match_id=_id,
                    radiant_heroes=[Hero(pick) for pick in radiant_pick],
                    dire_heroes=[Hero(pick) for pick in dire_pick],
                    winner=winner_side
                )
            )

    def __repr__(self):
        return "\n".join([str(_match) for _match in self.matches])


if __name__ == "__main__":
    md = MatchData()
    for i in range(1, 21):
        md.get_match_list(i)
    to_dump = 'radiant,dire,winner\n'
    for i in range(len(md.matches)):
        match = md.matches[i]
        dire_score = match.calculate_team_score(match.dire, match.radiant)
        radiant_score = match.calculate_team_score(match.radiant, match.dire)
        logger.info(f"Match {match.id} (winner {match.winner}): "
                    f"radiant {radiant_score:.4f} : {dire_score:.4f} dire")
        to_dump += f"{radiant_score}, {dire_score}, {0 if match.winner == 'radiant' else 1}\n"

    with open('collected_matches.csv', 'w') as file:
        file.write(to_dump)
