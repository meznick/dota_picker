# DOTA 2 PICKER
Predict match winner using picks.

## Data source
All hero matchups are taken from dotabuff /hero/counters page.

Mathes: /matches

## Predictions
Various methods:
- classification problem
- prediction by neural network

## Current state
Collecting data for single hero + its versus stats. Collecting list of matches
with lineup data.

### Team scoring
Team's score includes:
- team's side (todo): radiant or dire
- team's heroes scores

### Hero scoring
- score against anemies
- score featuring allied heroes (todo)

Future steps:
- parsing learning examples
- performing classification by classic methods
- preforming classification by neural networks